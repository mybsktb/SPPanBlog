/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.6.24 : Database - blog
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`blog` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `blog`;

/*Table structure for table `sys_blog` */

DROP TABLE IF EXISTS `sys_blog`;

CREATE TABLE `sys_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `content` text,
  `user_id` int(11) NOT NULL,
  `view_count` int(11) DEFAULT '0',
  `blog_type` int(1) DEFAULT '0',
  `share_url` varchar(128) DEFAULT NULL,
  `del_status` int(1) DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `desc` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

/*Data for the table `sys_blog` */

insert  into `sys_blog`(`id`,`title`,`content`,`user_id`,`view_count`,`blog_type`,`share_url`,`del_status`,`create_time`,`update_time`,`desc`) values (1,'CentOS6.3三种安装方法（U盘，硬盘，光盘）','<h2 style=\"margin: 15px auto 2px; padding: 0px; font-size: 21px; line-height: 1.5; font-family: Helvetica, Verdana, Arial, sans-serif; white-space: normal;\"><strong style=\"margin: 0px; padding: 0px;\">一、U盘安装步骤（推荐用这种方法）</strong></h2><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">使用到的材料：</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　1、CentOS-6.3-x86_64-bin-DVD1.iso</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　2、UltraISO</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　3、U盘一个</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">开始安装：</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　注意：使用UltraISO制作U盘启动的时候，必须在那个需要安装系统的电脑上面制作，如果你在A电脑上制作了，然后跑到B电脑上安装的时候会报一个错，这个貌似是CentOS系统的一个BUG，估计要后面的版本会改进。我就是卡在这里好久好久没装上。。。。o(╯□╰)o</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　1、打开UltraISO，依次点击“文件”&gt;“打开”，选择“CentOS-6.3-x86_64-bin-DVD1.iso”文件。</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　2、“启动”&gt;“写入硬盘映像”，在“硬盘驱动器选择u盘”，写入方式默认即可，点击“格式化”格式u盘，最后“写入”即可完成。</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　3、完成写入后，只保留“images”和“isolinux”两个文件夹，其余的全部删除，然后复制CentOS-6.3-x86_64-bin-DVD1.iso到u盘根目录。</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　4、重启以u盘启动,选”install system with basic driver”回车，语言选择“Chinese（Simplified）”回车，选择键盘模式，默认，然后回车。在下一步“Installation Method”选择“Hard drive”，然后选择u盘所在的分区（不确定的可以一个个尝试）。下面就是常规的系统安装了。</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　5，装完后，重启电脑到window7，打开EasyBCD，点add new entry，选Linux/BSD系统，把刚刚给/boot挂载的200M选中，然后点击添加就可以了。</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　6，重启电脑，应该就有了centos启动选项了。</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">&nbsp;</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">&nbsp;</p><h2 style=\"margin: 15px auto 2px; padding: 0px; font-size: 21px; line-height: 1.5; font-family: Helvetica, Verdana, Arial, sans-serif; white-space: normal;\"><strong style=\"margin: 0px; padding: 0px;\">二、硬盘安装步骤</strong></h2><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　1，腾出一个或者重新划分一个E盘，大小定义为10G就可以了，注意一定要小于32G。用FAT32格式化这个盘。</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　2，下载DVD镜像文件centos6.3***-dvd.iso，放入E盘，把里面的ioslinux文件夹中的vmlinuz和initrd.img文件，以及images文件夹提取出来，解压到和CentOS镜像文件相同的目录。解压完后E盘应该有四个文件：iso，vmlinz，initrd.img，images<span style=\"margin: 0px; padding: 0px; color: rgb(255, 0, 0);\">（注意：如果系统ISO文件有多个的话，必须要将它们都复制过来这个目录下，否则会在安装时提示“缺失ISO9660光盘”什么的！！！）</span></p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　3，Fedora 17时候操作跟centos一样，只不过不需要解压images文件夹了。</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　4. 打开EasyBCD，Add New Entry –&gt; NeoGrub — &gt; Install –&gt; Configure， 这时会弹出一个txt文件，在其中输入</p><pre style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; white-space: pre-wrap; word-wrap: break-word; font-family: &#39;Courier New&#39; !important;\">&nbsp;title&nbsp;install&nbsp;centos6.3\r\n&nbsp;\r\n&nbsp;kernel&nbsp;(hd0,4)/vmlinuz\r\n&nbsp;\r\n&nbsp;initrd&nbsp;(hd0,4)/initrd.img</pre><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　注：因为我装windows7的时候系统自带分了一个100M的启动分区，代号为hd0，那么C盘代号就为hd0,1了，以此类推，我的Win 7 分成了C盘系统盘和D盘，那么E盘为逻辑分区第一个分区，那么就是hd0,4了。</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　5，关闭，重启后选择 centos，选择centos6.3***-dvd.iso文件所在的目录，然后出现安装菜单的时候一定要选择第二个。</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\"><strong style=\"margin: 0px; padding: 0px;\">&nbsp;</strong></p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\"><strong style=\"margin: 0px; padding: 0px;\">&nbsp;</strong></p><h2 style=\"margin: 15px auto 2px; padding: 0px; font-size: 21px; line-height: 1.5; font-family: Helvetica, Verdana, Arial, sans-serif; white-space: normal;\"><strong style=\"margin: 0px; padding: 0px;\">三、光盘安装步骤</strong></h2><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　1，出现安装菜单的时候一定要选择第二个，貌似是”install system with basic driver”什么的</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　2，时区选择不要勾选UTC</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　3，分250M给/boot，80G给/，80G给/home，2G给swap</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　4，到选择引导程序的时候，点change device, 选择刚刚设定的/boot挂载点</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　5，装完后，重启电脑到window7，打开EasyBCD，点add new entry，选Linux/BSD系统，把刚刚给/boot挂载的200M选中，然后点击添加就可以了。</p><p style=\"margin: 10px auto; padding: 0px; font-family: Helvetica, Verdana, Arial, sans-serif; font-size: 14px; line-height: 22.4px; white-space: normal;\">　　6，重启电脑，应该就有了centos启动选项了。</p><p><br/></p>',1,31,1,'http://www.cnblogs.com/sbaicl/articles/2736416.html',0,'2016-10-27 13:47:03','2016-11-22 21:11:01','一、U盘安装步骤（推荐用这种方法）使用到的材料：　　1、CentOS-6.3-x86_64-bin-DVD1.iso　　2、UltraISO　　3、U盘一个开始安装：　　注意：使用UltraISO制作U盘启动的时候，必须在那个需要安装系统的电脑上面制作，如果你在A电脑上制作了，然后跑到B电脑上安装的时候会报一个错，这个貌似是CentOS系统的一个BUG，估计要后面的版本会改进。我就是卡在这里好久好久没装上。。。。o(╯□╰)o　　1、打开UltraISO，依次点击“文件”&gt;“打开”，选择“CentOS-6.3-x86_64-bin-DVD1.iso”文件。　　2、“启动”&gt;“写入硬...');

/*Table structure for table `sys_blog_tag` */

DROP TABLE IF EXISTS `sys_blog_tag`;

CREATE TABLE `sys_blog_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `sys_blog_tag` */

insert  into `sys_blog_tag`(`id`,`blog_id`,`tag_id`) values (1,1,1),(2,1,2),(3,1,7);

/*Table structure for table `sys_res` */

DROP TABLE IF EXISTS `sys_res`;

CREATE TABLE `sys_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `name` varchar(111) DEFAULT NULL,
  `des` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `iconCls` varchar(255) DEFAULT 'am-icon-file',
  `seq` int(11) DEFAULT '1',
  `type` int(1) DEFAULT '2' COMMENT '1 功能 2 权限',
  `modifydate` timestamp NULL DEFAULT NULL,
  `enabled` int(1) DEFAULT '1' COMMENT '是否启用 1：启用  0：禁用',
  `level` int(11) DEFAULT '0',
  `key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `sys_res` */

insert  into `sys_res`(`id`,`pid`,`name`,`des`,`url`,`iconCls`,`seq`,`type`,`modifydate`,`enabled`,`level`,`key`) values (1,NULL,'系统管理','系统管理','/admin/sys','am-icon-certificate',3,1,'2016-11-20 18:04:39',1,0,'admin:sys'),(2,1,'资源管理',NULL,'/admin/sys/res/list','',1,1,NULL,1,1,'admin:sys:res:list'),(3,1,'角色管理',NULL,'/admin/sys/role/list','',2,1,NULL,1,1,'admin:sys:role:list'),(4,1,'用户管理',NULL,'/admin/sys/user/list','',3,1,NULL,1,1,'admin:sys:user:list'),(5,4,'用户删除',NULL,'/admin/sys/user/delete','',1,2,NULL,1,2,'admin:sys:user:delete'),(6,4,'用户保存',NULL,'/admin/sys/user/save','',2,2,NULL,1,0,'admin:sys:user:save'),(7,2,'资源删除',NULL,'/admin/sys/res/delete','',1,2,NULL,1,0,'admin:sys:res:delete'),(8,2,'资源保存',NULL,'/admin/sys/res/save','',2,2,NULL,1,0,'admin:sys:res:save'),(9,3,'角色删除',NULL,'/admin/sys/role/delete','',1,2,NULL,1,0,'admin:sys:role:delete'),(10,3,'角色保存',NULL,'/admin/sys/role/save','',2,2,NULL,1,0,'admin:sys:role:save'),(13,NULL,'控制台','控制台','/admin/index','am-icon-home',1,1,'2015-02-10 16:09:40',1,0,'admin:index'),(14,13,'欢迎',NULL,'/admin/index','',1,1,NULL,1,1,'admin:index'),(15,NULL,'内容管理','内容管理','/admin/content','am-icon-book',2,1,NULL,1,0,'admin:content'),(16,15,'博客管理',NULL,'/admin/content/blog/list','',1,1,NULL,1,1,'admin:content:blog:list'),(17,16,'博客保存',NULL,'/admin/content/blog/save','',1,2,NULL,1,0,'admin:content:blog:save'),(18,16,'博客删除',NULL,'/admin/content/blog/delete','',1,2,NULL,1,0,'admin:content:blog:delete'),(19,15,'标签管理',NULL,'/admin/content/tags/list','',2,1,NULL,1,1,'admin:content:tags:list');

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL DEFAULT '',
  `des` varchar(55) DEFAULT NULL,
  `seq` int(11) DEFAULT '1',
  `createdate` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '0-禁用  1-启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`name`,`des`,`seq`,`createdate`,`status`) values (1,'admin','超级管理员',1,'2016-11-19 22:00:27',1),(88,'user','普通权限用户',1,'2016-11-22 08:54:48',1);

/*Table structure for table `sys_role_res` */

DROP TABLE IF EXISTS `sys_role_res`;

CREATE TABLE `sys_role_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sys_ROLE_RES_RES_ID` (`res_id`),
  KEY `FK_sys_ROLE_RES_ROLE_ID` (`role_id`),
  CONSTRAINT `sys_role_res_ibfk_1` FOREIGN KEY (`res_id`) REFERENCES `sys_res` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_role_res_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4370 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `sys_role_res` */

insert  into `sys_role_res`(`id`,`res_id`,`role_id`) values (4313,1,1),(4314,2,1),(4315,7,1),(4316,8,1),(4317,3,1),(4318,9,1),(4319,10,1),(4320,4,1),(4321,5,1),(4322,6,1),(4323,13,1),(4324,14,1),(4325,15,1),(4326,16,1),(4327,17,1),(4328,18,1),(4329,19,1),(4362,1,88),(4363,2,88),(4364,3,88),(4365,4,88),(4366,13,88),(4367,15,88),(4368,16,88),(4369,19,88);

/*Table structure for table `sys_tags` */

DROP TABLE IF EXISTS `sys_tags`;

CREATE TABLE `sys_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `sys_tags` */

insert  into `sys_tags`(`id`,`tag_name`) values (1,'JFinal'),(2,'Jnode'),(3,'标签1'),(4,'测测'),(5,'123'),(6,'Linux'),(7,'Test'),(8,'12'),(9,'12334');

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `des` varchar(55) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '#1 不在线 2.封号状态 ',
  `icon` varchar(255) DEFAULT '/images/guest.jpg',
  `email` varchar(50) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`name`,`pwd`,`des`,`status`,`icon`,`email`,`createdate`,`phone`) values (1,'admin','c78b6663d47cfbdb4d65ea51c104044e','超级管理员',1,'/sys/static/i/9.jpg','admin@admin.com','2016-11-23 10:04:51','15923935295'),(34,'test','c78b6663d47cfbdb4d65ea51c104044e','dfsdfsdfddf ',1,'/images/guest.jpg','569165857@qq.com','2016-11-22 08:55:34','13332892938');

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SYSTME_USER_ROLE_USER_ID` (`user_id`),
  KEY `FK_SYSTME_USER_ROLE_ROLE_ID` (`role_id`),
  CONSTRAINT `sys_user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `sys_user_role` */

insert  into `sys_user_role`(`id`,`user_id`,`role_id`) values (3,34,88),(7,1,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
