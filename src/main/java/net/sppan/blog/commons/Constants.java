package net.sppan.blog.commons;

public class Constants {
	
	/**
	 * 缓存枚举
	 */
	public enum CacheName {
		menu,//菜单
		session,//用户数据
		top,//热门信息
		blog,//博客
		defaultCache;
		public String get() {
			return this.name();
		}
	}

}
