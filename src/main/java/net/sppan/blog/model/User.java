package net.sppan.blog.model;

import java.util.List;
import java.util.Set;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.plugin.ehcache.IDataLoader;

import net.sppan.blog.commons.Constants;
import net.sppan.blog.model.base.BaseUser;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class User extends BaseUser<User> {
	public static final User dao = new User();

	public List<User> findUserByRoleId(Integer id) {
		return Db.query("SELECT u.* FROM sys_user AS u,sys_user_role AS r WHERE u.id = r.user_id AND r.role_id =? ", id);
	}

	/**
	 * 从缓存中加载用户
	 * @param userId
	 * @return
	 */
	public User loadInSession(String userId) {
		if (StrKit.isBlank(userId)) {
			return null;
		}
		return loadInSession(Long.parseLong(userId));
	}

	/**
	 * 从缓存中加载用户
	 * @param userId
	 * @return
	 */
	public User loadInSession(final long userId) {
		return CacheKit.get(Constants.CacheName.session.get(), userId, new IDataLoader() {

			@Override
			public Object load() {
				return findById(userId);
			}
		});
	}

	/**
	 * 权限集
	 */
	public Set<String> getPermissionSets() {
		return Res.dao.getUserAllResKeySet(this.getId());
	}

	/**
	 * 用户登录
	 * @param userName
	 * @param pwd
	 * @return
	 */
	public User login(String userName, String pwd) {
		return this.findFirst("select * from sys_user where name = ? and pwd = ? and status = 1 limit 1", userName, pwd);
	}
	
	
}
