package net.sppan.blog.model;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;

import net.sppan.blog.model.base.BaseRole;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Role extends BaseRole<Role> {
	public static final Role dao = new Role();

	/**
	 * 保存角色关联的资源信息
	 * @param menuIds
	 * @param roleId
	 */
	public void saveMenuAssign(String menuIds, Integer roleId) {
		Db.update("delete from sys_role_res where role_id = ?", roleId);
		if(StrKit.notBlank(menuIds)){//改成批量插入
			List<String> sqlList= new ArrayList<String>();
			for(String id : menuIds.split(",")){
				if(StrKit.notBlank(id)){
					if(!Integer.valueOf(id).equals(10000))
						sqlList.add("insert into sys_role_res (role_id,res_id) values ("+roleId+","+Integer.valueOf(id)+")");
				}
			}
			Db.batch(sqlList, 50);
		}
	}

	/**
	 * 根据用户ID获取用户属于的角色列表
	 * @param id
	 * @return
	 */
	public Role findRoleByUserId(Integer id) {
		return findFirst("SELECT r.* FROM sys_role AS r ,sys_user_role AS u WHERE r.id = u.role_id AND u.user_id = ?",id);
	}

	/**
	 * 更新用户对应得角色信息
	 * @param roleId
	 * @param userId
	 * @return
	 */
	public Integer updateRoleByUserId(Integer roleId, Integer userId) {
		Db.update("delete from sys_user_role where user_id = ?",userId);
		int count = Db.update("insert sys_user_role(role_id, user_id) values (?,?)",roleId,userId);
		return count;
	}
}
