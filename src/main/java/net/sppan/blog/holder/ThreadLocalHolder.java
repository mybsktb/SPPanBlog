package net.sppan.blog.holder;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ThreadLocalHolder {
	public static ThreadLocal<Map<String, Object>> local =new ThreadLocal<Map<String, Object>>();
	public static final String _REQUEST = "_request";
	public static final String _RESPONSE = "_response";
	
	public static HttpServletRequest getRequest(){
		return (HttpServletRequest) local.get().get(_REQUEST);
	}

	public static HttpServletResponse getResponse(){
		return (HttpServletResponse)local.get().get(_RESPONSE);
	}
}
