package net.sppan.blog.controller.admin;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;

import net.sppan.blog.annotation.RequiresPermissions;
import net.sppan.blog.controller.BaseController;
import net.sppan.blog.model.Role;
import net.sppan.blog.model.RoleRes;
import net.sppan.blog.model.User;
import net.sppan.blog.model.base.Condition;
import net.sppan.blog.model.base.Operators;

@RequiresPermissions(key={"admin:sys:role"})
public class AdminRoleController extends BaseController{
	/**
	 * 角色列表
	 */
	@RequiresPermissions(key={"admin:sys:role:list"})
	public void list(){
		int pageNumber = getParaToInt("pageNumber", 1);
        int pageSize = getParaToInt("pageSize", 10);
        String search = getPara("search");
        LinkedHashMap<String, String> orderby = new LinkedHashMap<String, String>();
        orderby.put("seq", "asc");
        Set<Condition> conditions=new HashSet<Condition>();
        if(StrKit.notBlank(search)){
        	conditions.add(new Condition("name", Operators.LIKE, search));
        	setAttr("search", search);
        }
		Page<Role> page = Role.dao.getPage(pageNumber, pageSize, conditions, orderby);
		setAttr("pageList", page);
		render("list.html");
	}
	
	/**
	 * 跳转到表单页面
	 */
	@RequiresPermissions(key={"admin:sys:role:save"})
	public void add_edit(){
		Integer id = getParaToInt("id");
		if(id != null){
			Role role = Role.dao.findById(id);
			setAttr("role", role);
		}
		render("form.html");
	}
	/**
	 * 新增-更新角色
	 */
	@RequiresPermissions(key={"admin:sys:role:save"})
	public void save_update(){
		Role role = getModel(Role.class);
		role.setCreatedate(new Date());
		if(role.getId() == null){
			renderJson(role.save(),"新增成功");
		}else{
			renderJson(role.update(),"编辑成功");
		}
	}
	
	/**
	 * 删除角色
	 */
	@RequiresPermissions(key={"admin:sys:role:delete"})
	public void delete(){
		Integer id = getParaToInt("id");
		Role role = Role.dao.findById(id);
		List<User> users = User.dao.findUserByRoleId(id);
		boolean isSuccess  = false;
		if(1 ==role.getId()){
			renderJson(isSuccess,"超级管理员不能删除");
		}else if(users != null && users.size()>0){
			renderJson(isSuccess,"该角色还关联了用户，不能进行删除");
		}else{
			Integer count = RoleRes.dao.deleteByRoleId(id);
			if (count != null && count >= 0) {
				isSuccess = Role.dao.deleteById(id);
				renderJson(isSuccess,"删除成功");
			}else{
				renderJson(isSuccess,"删除失败");
			}
		}
	}
	
	/**
	 * 跳转权限赋予页面
	 */
	@RequiresPermissions(key={"admin:sys:role:save"})
	public void grant(){
		Integer roleId = getParaToInt("roleId");
		Integer type = getParaToInt("type");
		setAttr("roleId", roleId);
		setAttr("type", type);
		render("grant.html");
	}
	
	/**
	 * 保存赋予角色的权限
	 */
	@RequiresPermissions(key={"admin:sys:role:save"})
	public void saveMenuAssign(){
		Integer roleId=this.getParaToInt("roleId");
		String menuIds=this.getPara("menuIds");
//		if(roleId == 1){
//			renderJson(false, "超级管理员的权限不能修改");
//		}else{
			Role.dao.saveMenuAssign(menuIds, roleId);
			renderJson("isSuccess", true);
//		}
	}
}
