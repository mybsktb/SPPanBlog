package net.sppan.blog.controller;

import net.sppan.blog.controller.web.BlogController;
import net.sppan.blog.controller.web.IndexController;

import com.jfinal.config.Routes;

public class WebRouters extends Routes {

	@Override
	public void config() {
		add("/", IndexController.class,"/web");
		add("/blog", BlogController.class,"/web/blog");
	}

}
